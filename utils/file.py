'''
Thanks to <Sridhar Ratnakumar> for this function
Source: <https://stackoverflow.com/a/1094933>
'''
def humanize_size(num, suffix='B'):
    try:
        num = int(num)
        for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Yi', suffix)
    except:
        return num
