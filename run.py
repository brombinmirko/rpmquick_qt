import os, sys, getopt, subprocess, random, utils
from PyQt5.QtWidgets import QMainWindow, QApplication, QCheckBox, QComboBox, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QPlainTextEdit, QLineEdit, QTreeView, QVBoxLayout, QWidget, QPushButton, QAction, QVBoxLayout, QTabWidget
from PyQt5.QtGui import QIcon, QStandardItemModel, QPixmap
from PyQt5.QtCore import QDate, QDateTime, QRegExp, QSortFilterProxyModel, Qt, QTime, pyqtSlot
from PIL import Image

class App(QMainWindow):
    
    def __init__(self, package):
        super().__init__()
        self.package = package
        self.title = 'RPM Quick'
        self.width = 600
        self.height = 500
        self.initUI()
        
    def initUI(self):
        package_path = os.path.abspath(self.package)
        tmp_path = '/tmp/rpmquick/%s/' % random.randint(374291,982361823)
        
        # RPM Extract
        subprocess.run(['mkdir', '-p', tmp_path])
        subprocess.check_output('cd {tmp_path} ; rpm2cpio {package_path} | cpio -idmv'.format(
                tmp_path = tmp_path,
                package_path = package_path
            ), shell=True)
        
        # RPM Icon
        self.rpm_icon = "data/package_icon.png"
        for dirpath, dirnames, filenames in os.walk(tmp_path):
            for filename in [f for f in filenames if f.endswith(".png")]:
                icon_img = Image.open(os.path.join(dirpath, filename))
                if "icon" in os.path.join(dirpath, filename) or "logo" in os.path.join(dirpath, filename):
                    if icon_img.size[0] >= 48:
                        self.rpm_icon = os.path.join(dirpath, filename)
                        break
        # RPM Files
        command = ['rpm', '-ql', package_path]
        p = subprocess.run(command, stdout=subprocess.PIPE)
        self.rpm_files = p.stdout.decode('utf-8', 'ignore').split("\n")
        
        # RPM Dependencies
        command = ['rpm', '-q', '--requires', package_path]
        p = subprocess.run(command, stdout=subprocess.PIPE)
        self.rpm_dependencies = p.stdout.decode('utf-8', 'ignore').split("\n")
        
        # RPM Info
        '''
        This method must be changed.
        The list does not always have the same 
        number of values. We need to create a 
        function that identifies the items by 
        their prefix (Vendor:, ..)
        '''
        command = ['rpm', '-qi', package_path]
        p = subprocess.run(command, stdout=subprocess.PIPE)
        self.rpm_info = p.stdout.decode('utf-8', 'ignore').split("\n")
        i = 0
        for ri in self.rpm_info:
            self.rpm_info[i] = ri.split(':', 1)[-1][1:]
            i+=1
        
        self.setWindowTitle(self.title)
        self.setGeometry(0, 0, self.width, self.height)
        
        frame = self.frameGeometry()
        screen = QApplication.desktop().screenNumber(QApplication.desktop().cursor().pos())
        centerPoint = QApplication.desktop().screenGeometry(screen).center()
        frame.moveCenter(centerPoint)
        self.move(frame.topLeft())
        
        self.tabs = Tabs(self)
        self.setCentralWidget(self.tabs)
        
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        helpMenu = mainMenu.addMenu('Help')
        
        openButton = QAction(QIcon('open'), 'Open', self)
        openButton.setShortcut('Ctrl+O')
        openButton.setStatusTip('Open RPM package')
        openButton.triggered.connect(self.close)
        
        exitButton = QAction(QIcon('close'), 'Exit', self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip('Exit application')
        exitButton.triggered.connect(self.close)
        
        fileMenu.addAction(openButton)
        fileMenu.addAction(exitButton)
    
        self.show()
    
class Tabs(QWidget):
    
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout(self)
        
        self.package_files(parent)
        self.package_dependencies(parent)
        self.package_installation(parent)
        
        # Initialize tab screen
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tabs.resize(300,200)
        
        # Create tabs
        self.tabs.addTab(self.tab1,"Info")
        self.tabs.addTab(self.tab2,"Dependencies")
        self.tabs.addTab(self.tab3,"Files")

        # Build third tab
        self.tab3.layout = QVBoxLayout(self)
        self.tab3.layout.addWidget(self.rpm_data_files)
        self.tab3.setLayout(self.tab3.layout)

        # Build second tab
        self.tab2.layout = QVBoxLayout(self)
        self.tab2.layout.addWidget(self.dataGroupBox)
        self.tab2.setLayout(self.tab2.layout)

        # Build first tab
        self.tab1.layout = QVBoxLayout(self)
        self.tab1.layout.addWidget(self.pkg_info)
        self.tab1.setLayout(self.tab1.layout)
        
        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)
    
    '''
    Create a list with package files
    '''
    def package_files(self, parent):
        self.FileName, self.FileStatus = range(2)
        self.rpm_data_files = QGroupBox("Package files")
        self.rpm_data_files_view = QTreeView()
        self.rpm_data_files_view.setRootIsDecorated(False)
        self.rpm_data_files_view.setAlternatingRowColors(True)
        
        dataLayout = QHBoxLayout()
        dataLayout.addWidget(self.rpm_data_files_view)
        self.rpm_data_files.setLayout(dataLayout)
        
        model = self.create_rpm_data_file_model(self)
        self.rpm_data_files_view.setModel(model)
        for rf in parent.rpm_files:
            if rf != "":
                self.add_rpm_data_file(model, rf)
    
    def create_rpm_data_file_model(self, parent):
        model = QStandardItemModel(0, 1, parent)
        model.setHeaderData(self.FileName, Qt.Horizontal, "Name")
        return model
    
    def add_rpm_data_file(self, model, file_name):
        model.insertRow(0)
        model.setData(model.index(0, self.FileName), file_name)
    
    '''
    Create a list with dependencies and their sizes
    '''
    def package_dependencies(self, parent):
        self.Name, self.Status = range(2)
        self.dataGroupBox = QGroupBox("Package dependencies")
        self.dataView = QTreeView()
        self.dataView.setRootIsDecorated(False)
        self.dataView.setAlternatingRowColors(True)
        
        dataLayout = QHBoxLayout()
        dataLayout.addWidget(self.dataView)
        self.dataGroupBox.setLayout(dataLayout)
        
        model = self.createDependenceModel(self)
        self.dataView.setModel(model)
        for rd in parent.rpm_dependencies:
            if rd != "":
                self.addDependence(model, rd)
    
    def createDependenceModel(self, parent):
        model = QStandardItemModel(0, 1, parent)
        model.setHeaderData(self.Name, Qt.Horizontal, "Name")
        return model

    def addDependence(self, model, name):
        model.insertRow(0)
        model.setData(model.index(0, self.Name), name)
    
    '''
    Create grid with data for the package installation
    '''
    def package_installation(self, parent):
        # Package icon
        pkg_icon = QLabel(self)
        print(parent.rpm_icon)
        pkg_icon_pix = QPixmap(parent.rpm_icon).scaled(64, 64)
        pkg_icon.setPixmap(pkg_icon_pix)
        
        # Package name 
        pkg_name = QLabel(self)
        pkg_name.setText(parent.rpm_info[0])
        pkg_name.setStyleSheet("font-size: 20pt")
        
        # Package vendor 
        pkg_vendor = QLabel(self)
        pkg_vendor.setText("by {vendor}".format(vendor = parent.rpm_info[14]))
        
        # Package version 
        pkg_version = QLabel(self)
        pkg_version.setText("v.{version} <span style='color: #afafaf'>{arch}</span>".format(
                version = parent.rpm_info[1], 
                arch = parent.rpm_info[3]
            )
        )
        
        # Package url 
        pkg_url = QLabel(self)
        pkg_url.setText("<a href='{link}'>{link}</a>".format(link=parent.rpm_info[15]))
        pkg_url.setOpenExternalLinks(True)
        
        # Package size 
        pkg_size = QLabel(self)
        pkg_size.setText("Size: {size}".format(size = utils.file.humanize_size(parent.rpm_info[6])))
        
        # Package build date 
        pkg_date = QLabel(self)
        pkg_date.setText("Build date: {build}".format(build = parent.rpm_info[10]))
        
        # Package group
        pkg_group = QLabel(self)
        pkg_group.setText(parent.rpm_info[5])
        
        # Package install button 
        pkg_install = QPushButton(self)
        pkg_install.setText('Install')
        
        # Package cancel button 
        pkg_cancel = QPushButton(self)
        pkg_cancel.setText('Cancel')
        
        # Package grid
        pkg_grid = QGridLayout()
        pkg_grid.setColumnStretch(0, 2)
        pkg_grid.setColumnStretch(1, 4)
        pkg_grid.setColumnStretch(2, 4)
        pkg_grid.addWidget(pkg_icon,0,0)
        pkg_grid.addWidget(pkg_name,0,1)
        pkg_grid.addWidget(pkg_vendor,1,1)
        pkg_grid.addWidget(pkg_version,2,1)
        pkg_grid.addWidget(pkg_group,3,1)
        pkg_grid.addWidget(pkg_url,4,1)
        pkg_grid.addWidget(pkg_size,5,1)
        pkg_grid.addWidget(pkg_date,6,1)
        pkg_grid.addWidget(pkg_install,7,1)
        pkg_grid.addWidget(pkg_cancel,7,2)
        
        # Package info box
        self.pkg_info = QGroupBox("Package installation")
        self.pkg_info.setLayout(pkg_grid)

def main(argv):
    package = ''
    try:
        opts, args = getopt.getopt(argv,"h:o:",["open="])
    except getopt.GetoptError:
        print('rpmquick -o <package.rpm>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-o", "--open"): # open
            package = arg
            app = QApplication(sys.argv)
            ex = App(package)
            sys.exit(app.exec_())
        else:
            print('rpmquick -o <package.rpm>')
            sys.exit()

if __name__ == '__main__':
    main(sys.argv[1:])
